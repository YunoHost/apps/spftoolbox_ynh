# spftoolbox_ynh

![](https://camo.githubusercontent.com/bd296757ad81cd20b5263db892f5bcf4ca0e7b16/687474703a2f2f692e696d6775722e636f6d2f4143785a5074512e706e67 "Screenshot")

See your record and check your domain settings

## How to install it : 

### Through CLI
`yunohost app install -u  https://github.com/Yunohost-Apps/spftoolbox_ynh`

### Through the Admin Web Interface
Just add the URL in the installation box.

## State

Working/Testing
